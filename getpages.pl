#!/usr/bin/perl

use Mojo::DOM;
use Mojo::UserAgent;

use feature 'say';

my $ua = Mojo::UserAgent->new;
use Mojo::Util qw(spurt);

foreach my $page (1..53) {
	print "$page: ";
	my $tx = $ua->post('http://www.ish2013.com/index.php' => form => {
		search_mode => 'manu',
		company => '',
		tour => '',
		manusearch => '',
		zip => '',
		city => '',
		country => 'Germany',
		hall => '',
		id => '26',
		mode => 'search_result',
		L => '',
		sortfield => '',
		sortasc => '0',
		page => $page,
		view => '0',
	});

	if (my $res = $tx->success) {
		spurt($res->body, "data/$page.html");
		say "ok";
#		say $res->body
	}
	else {
		my ($err, $code) = $tx->error;
		say $code ? "$code response: $err" : "Connection error: $err";
	}
	
	
}

