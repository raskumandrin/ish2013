#!/usr/bin/perl

use Mojo::DOM;
use Mojo::UserAgent;

use feature 'say';

my $ua = Mojo::UserAgent->new;
use Mojo::Util qw(slurp spurt);

my $i=0;

foreach my $page (1..53) {
	Mojo::DOM->new( slurp("data/$page.html") )->find('tr td:nth-child(2) a')->each(sub {
		my $a = shift;
		$i++;
		say "$i: $a->{href}";
		$ua->max_redirects(5)->get("http://www.ish2013.com/$a->{href}")
		  ->res->content->asset->move_to("firms/$i.html");
	});

}

