#!/usr/bin/perl

use Mojo::DOM;
use Mojo::UserAgent;

use feature 'say';

my $ua = Mojo::UserAgent->new;
use Mojo::Util qw(slurp spurt);

%url;

open(my $fh,'<','list.txt');
while (<$fh>) {
	chomp;
	my ($n,$u) = ( /(\d+): (.*)/ );
	$url{$n} = 'http://www.ish2013.com/'.$u;
}
close $fh;

foreach my $num (1..1056) {
	my %c;

	my $dom = Mojo::DOM->new( slurp("firms/$num.html") );
	
	$dom->find('div.manufacturer_detail_preview strong')->each(sub {
		$c{name} = shift->text();
	});
	
	
	my $addr;
	my $count;
	if ( $dom->find('div.manufacturer_detail_preview div')->size ) {
		my $firm = $dom->find('div.manufacturer_detail_preview div')->to_string;
		
		unless ($c{name}) {
			( $c{name} ) = ( $firm =~ /(.+)<br/ );
		}
		
		
		$firm =~ s/<br\s\/>/ /g;
		($addr) = ( $firm =~ /<\/strong>([^<]+)<img/ );

		my @a = split(/\(Germany\)/,$addr);
		
		if ($a[0]) {
			( $c{str1},$c{ind1},$c{cit1} ) = ($a[0] =~ /(.*) (\d{5}) (.*)/ );
		}
		if ($a[1]) {
			( $c{str2},$c{ind2},$c{cit2} ) = ($a[1] =~ /(.*) (\d{5}) (.*)/ );
			
			if ($c{str2} or $c{ind2} or $c{cit2}) {
				$c{c2} = 'Germany';
			}
			
		}
		
#		$c{a0} = $a[0];
#		$c{a1} = $a[1];

		
		( $c{phone} ) = ($firm =~ /icon_tel\.gif[^>]+>([^<]+)<img/);
		( $c{fax} ) = ($firm =~ /faxsym_g\.png[^>]+>([^<]+)<img/);
		( $c{mail} ) = ($firm =~ /icon_mail\.gif[^>]+>(.+)<img/);
		if ( $c{mail} =~ /<\/a>/ ) {
			( $c{mail} ) = ( $c{mail} =~ /">(.+)<\/a>/);
		};
		( $c{url} ) = ($firm =~ /icon_url\.gif[^>]+>(.+)<\/div/);
		if ( $c{url} =~ /<\/a>/ ) {
			( $c{url} ) = ( $c{url} =~ /">(.+)<\/a>/);
		};
	}
	
#	say "$num: $count" if $count>4;

#	say "$num";

#	say "\t0: $c{str1} ||| $c{ind1} ||| $c{cit1}";
#	say "\t1: $c{str2} ||| $c{ind2} ||| $c{cit2}";
#	say "\tname: $c{name}";
#	say "\taddr: $addr";
#	say "\tphone: $c{phone}";
#	say "\tfax: $c{fax}";
#	say "\tmail: $c{mail}";
#	say "\turl: $c{url}";

foreach my $key (qw(str1 ind1 cit1 str2 ind2 cit2 phone fax mail url)){
	$c{$key} =~ s/\s+$//g;
	$c{$key} =~ s/,$//g;
	$c{$key} =~ s/\s+$//g;
	$c{$key} =~ s/^\s+//g;
	$c{$key} =~ s/^,//g;
	$c{$key} =~ s/^\s+//g;
	
}


if ($num == 242) {
	$c{name} = 'DWA Landesverband Hessen/Rheinland-Pfalz/Saarland e.V.';
	$c{ind1} = '1520108';
	$c{ind2} = '1520106';
	$c{c2} = 'Germany';
}

if ($num == 875) {
	$c{str1} = 'Bültestr. 70 b';
	$c{ind1} = '32584';
	$c{cit1} = 'Löhne';
}

if ($num == 875) {
	$c{str1} = 'Bültestr. 70 b';
	$c{ind1} = '32584';
	$c{cit1} = 'Löhne';
}

if ($num == 114) {
	$c{mail} = 'sklotz@rubinetteriebresciane.it, amartin@bonomigmbh.de, amartin@bonomigmbh.com';
	$c{url} = 'www.bonomi.it, www.bonomigmbh.com';
}
if ($num == 168) {
	$c{mail} = 'j.thomson@ceramtec.de, mechanical_systems@ceramtec.de';
	$c{url} = 'www.ceramtec.com/ish';
}

if ($num == 466) {
	$c{mail} = 'd.schaefer@industrilas.de, info@industrilas.de';
	$c{url} = 'www.klima-flex.de, www.industrilas.de';
}

if ($num == 534) {
	$c{mail} = 'a.montag@knipex.de, info@knipex.de';
	$c{url} = 'www.knipex.de';
}

if ($num == 598) {
	$c{mail} = 'info@maico.de, iris.hug@maico.de';
	$c{url} = 'www.maico-ventilatoren.com';
}
if ($num == 659) {
	$c{mail} = 'info@neoperl.de, info@neoperl.net';
	$c{url} = 'www.neoperl.net';
}
if ($num == 719) {
	$c{mail} = 'verkauf3@promat.de';
	$c{url} = 'www.promat.de, www.promat.de/twd';
}
if ($num == 782) {
	$c{mail} = 'messe@ruch.de, info@ruch.de';
	$c{url} = 'www.ruch.de';
}
if ($num == 786) {
	$c{mail} = 'maria.krueselmann@ruetgers.com, info@ruetgers.com';
	$c{url} = 'www.ruetgers.com';
}
if ($num == 972) {
	$c{mail} = 'info@vdzev.de';
	$c{url} = 'www.intelligent-heizen.info, www.vdzev.de';
}


print "$c{name}\t$c{str1}\t$c{ind1}\t$c{cit1}\tGermany\t$c{str2}\t$c{ind2}\t$c{cit2}\t$c{c2}\t$c{phone}\t$c{fax}\t$c{mail}\t$c{url}\t$url{$num}\n";

#		<img src="fileadmin/tinymce/img/icon_tel.gif" alt="" width="18" height="10">+49 (0)7431 1295-0,
#		<img class="mceIcon" src="fileadmin/tinymce/img/faxsym_g.png" alt="Symbol Fax">+49 (0)7431 1295-35<br>
#		<img src="fileadmin/tinymce/img/icon_mail.gif" alt="Symbol" width="18" height="10"> info@aat-online.de,
#		<img src="fileadmin/tinymce/img/icon_url.gif" alt="Symbol" width="18" height="10"> www.aat-online.de<br></div>
		
#	say $c{name};
#say $addr,$index;




#	say scalar @firm;
#	exit;
#	3P Technik Filtersysteme GmbH (COMPANY NAME)
#	Öschstr. 14 (STREET)
#	73072 (ZIP CODE))
#	Donzdorf (CITY)
#	Germany (COUNTRY)
#	+49 (0)7162 946070 (PHONE)
#	Fax+49 (0)7162 9460799 (FAX)
#	info@3ptechnik.de (EMAIL)
#	www.3ptechnik.de (URL)	

}

